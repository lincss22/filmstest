package soft_team.filmstest.Adapter;


import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import soft_team.filmstest.Activity.MainActivity_;
import soft_team.filmstest.R;
import soft_team.filmstest.Server.Transport;
import soft_team.filmstest.Server.servermodels.GetResults;


public class FilmListAdapter extends ArrayAdapter<GetResults> {
    final LayoutInflater mInflater;
    private Context mContext;
    public FilmListAdapter(Context context) {
        super(context, 0);
        mInflater = LayoutInflater.from(getContext());
        this.mContext=context;
    }

    @Override
    public View getView(final int position, View convertView,
                        final ViewGroup parent) {

        final ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            holder = new ViewHolder();
            rowView =  mInflater.inflate(R.layout.item_film_list, null, false);
            holder.info_la = (LinearLayout) rowView.findViewById(R.id.info_la);
            holder.image = (ImageView) rowView.findViewById(R.id.image);
            holder.filmName = (TextView) rowView.findViewById(R.id.filmName);
            holder.description = (TextView) rowView.findViewById(R.id.description);
            holder.rating = (TextView) rowView.findViewById(R.id.rating);
            holder.release = (TextView) rowView.findViewById(R.id.release);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        final GetResults item = getItem(position);
        if(!TextUtils.isEmpty(item.backdrop_path)){
            Picasso
                    .with(getContext())
                    .load(Transport.URL + item.backdrop_path)
                    .centerCrop()
                    .fit()
                    .into(holder.image);
        }
        if(!TextUtils.isEmpty(item.title)){
            holder.filmName.setText(item.title);
        }
        if(!TextUtils.isEmpty(item.overview)){
            holder.description.setText(item.overview);
        }
        if(!TextUtils.isEmpty(item.release_date)){
            holder.release.setText(String.valueOf(item.release_date));
        }
        if(!TextUtils.isEmpty(String.valueOf(item.vote_average))){
            holder.rating.setText(String.valueOf(item.vote_average));
        }
        return rowView;
    }

    static class ViewHolder {
        public LinearLayout info_la;
        public ImageView image;
        public TextView filmName;
        public TextView description;
        public TextView rating;
        public TextView release;
    }

}
