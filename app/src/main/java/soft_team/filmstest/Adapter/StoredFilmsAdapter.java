package soft_team.filmstest.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import soft_team.filmstest.Activity.DetailActivity_;
import soft_team.filmstest.Activity.MainActivity_;
import soft_team.filmstest.R;
import soft_team.filmstest.Realm.RealmManager;
import soft_team.filmstest.Realm.models.FilmsListModel;
import soft_team.filmstest.Server.Transport;
import soft_team.filmstest.Server.servermodels.GetResults;

public class StoredFilmsAdapter extends ArrayAdapter<FilmsListModel> {
    final LayoutInflater mInflater;
    private Context mContext;
    public StoredFilmsAdapter(Context context) {
        super(context, 0);
        mInflater = LayoutInflater.from(getContext());
        this.mContext=context;
    }

    @Override
    public View getView(final int position, View convertView,
                        final ViewGroup parent) {

        final FilmListAdapter.ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            holder = new FilmListAdapter.ViewHolder();
            rowView =  mInflater.inflate(R.layout.item_film_list, null, false);
            holder.info_la = (LinearLayout) rowView.findViewById(R.id.info_la);
            holder.image = (ImageView) rowView.findViewById(R.id.image);
            holder.filmName = (TextView) rowView.findViewById(R.id.filmName);
            holder.description = (TextView) rowView.findViewById(R.id.description);
            holder.rating = (TextView) rowView.findViewById(R.id.rating);
            holder.release = (TextView) rowView.findViewById(R.id.release);
            rowView.setTag(holder);
        } else {
            holder = (FilmListAdapter.ViewHolder) rowView.getTag();
        }

        final FilmsListModel item = getItem(position);
        if(!TextUtils.isEmpty(item.getBackdrop_path())){
            Picasso
                    .with(getContext())
                    .load(Transport.URL + item.getBackdrop_path())
                    .centerCrop()
                    .fit()
                    .into(holder.image);
        }
        if(!TextUtils.isEmpty(item.getTitle())){
            holder.filmName.setText(item.getTitle());
        }
        if(!TextUtils.isEmpty(item.getOverview())){
            holder.description.setText(item.getOverview());
        }
        if(!TextUtils.isEmpty(item.getRelease_date())){
            holder.release.setText(String.valueOf(item.getRelease_date()));
        }
        if(!TextUtils.isEmpty(String.valueOf(item.getVote_average()))){
            holder.rating.setText(String.valueOf(item.getVote_average()));
        }
        holder.info_la.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle(null);
                alert.setMessage(R.string.message_saved);
                alert.setPositiveButton(R.string.my_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DetailActivity_.intent(getContext()).extra("filmDataSaved",item).extra("stored",true).start();
                    }
                });
                alert.setNeutralButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        RealmManager.getInstance().deleteFilm(item.getMyId());
                        ((MainActivity_)mContext).loadGetFilms();
                    }
                });
                alert.setNegativeButton(R.string.my_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });
        return rowView;

    }

    static class ViewHolder {
        public LinearLayout info_la;
        public ImageView image;
        public TextView filmName;
        public TextView description;
        public TextView rating;
        public TextView release;
    }

}
