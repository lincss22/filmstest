package soft_team.filmstest.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class Settings {
    private static Context mContext = MainApplication_.getInstance().getApplicationContext();
    private static final String KEY_STORE_NAME = "Filmstest";
    private final static String APIKEY = "APIKEY";

    public static void setApikey(Context context, String number) {
        setString(APIKEY, number);
    }

    public static String getApikey(Context context) {
        return getString(APIKEY);
    }

    private static void setString(String key, String value) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(KEY_STORE_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    private static String getString(String key) {
        SharedPreferences settings = mContext.getSharedPreferences(KEY_STORE_NAME, Context.MODE_PRIVATE);
        return settings.getString(key, "");
    }
}
