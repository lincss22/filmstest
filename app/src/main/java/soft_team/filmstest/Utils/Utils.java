package soft_team.filmstest.Utils;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;

import soft_team.filmstest.Realm.RealmManager;
import soft_team.filmstest.Realm.models.DetailsFilmModel;
import soft_team.filmstest.Realm.models.FilmsListModel;
import soft_team.filmstest.Realm.models.GenersModel;
import soft_team.filmstest.Server.servermodels.GetResults;
import soft_team.filmstest.Server.servermodels.ResponseDetails;

public class Utils {
    public static FilmsListModel transform(GetResults getResults,DetailsFilmModel detailsFilmModel){
        FilmsListModel filmsListModel = new FilmsListModel();
        try {
            filmsListModel.setId(getResults.id);
            filmsListModel.setVote_average(getResults.vote_average);
            filmsListModel.setTitle(getResults.title);
            filmsListModel.setBackdrop_path(getResults.backdrop_path);
            filmsListModel.setOverview(getResults.overview);
            filmsListModel.setRelease_date(getResults.release_date);
            filmsListModel.setDetailsFilmModel(detailsFilmModel);
            return filmsListModel;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static DetailsFilmModel transform(ResponseDetails responseDetails){
        DetailsFilmModel detailsFilmModel = new DetailsFilmModel();
        try {
            detailsFilmModel.setId(responseDetails.id);
            detailsFilmModel.setVote_average(responseDetails.vote_average);
            detailsFilmModel.setTitle(responseDetails.title);
            detailsFilmModel.setBackdrop_path(responseDetails.backdrop_path);
            detailsFilmModel.setOverview(responseDetails.overview);
            detailsFilmModel.setRelease_date(responseDetails.release_date);
            detailsFilmModel.setBudget(responseDetails.budget);
            detailsFilmModel.setPoster_path(responseDetails.poster_path);
            detailsFilmModel.setHomepage(responseDetails.homepage);
            String geners = "";
            for(GenersModel g: responseDetails.genres){
                geners += g.name + "\n";
            }
            detailsFilmModel.setGenres(geners);
            return detailsFilmModel;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void checkButton(View v){
        if(RealmManager.getInstance().getFilmProfiles()!= null && !RealmManager.getInstance().getFilmProfiles().isEmpty()){
            v.setVisibility(View.VISIBLE);
        } else {
            v.setVisibility(View.GONE);
        }
    }
}
