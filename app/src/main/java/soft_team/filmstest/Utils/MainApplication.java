package soft_team.filmstest.Utils;

import android.app.Application;
import org.androidannotations.annotations.EApplication;

import io.realm.Realm;
import io.realm.RealmConfiguration;

@EApplication
public class MainApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(new LifecycleHandler());
        RealmConfiguration myConfig = new RealmConfiguration.Builder(getApplicationContext())
                .name("myrealm.realm")
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(myConfig);
    }
}
