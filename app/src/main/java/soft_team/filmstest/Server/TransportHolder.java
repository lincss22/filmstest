package soft_team.filmstest.Server;

public class TransportHolder {
    private static Transport.TransportService instance;
    public static Transport.TransportService getServiceInstance() {
        synchronized (TransportHolder.class) {
            if(instance == null) {
                instance = Transport.newInstance();
            }
        }
        return instance;
    }
}
