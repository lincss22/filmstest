package soft_team.filmstest.Server.servermodels;

import java.util.ArrayList;

import soft_team.filmstest.Realm.models.GenersModel;

public class ResponseDetails {
    public int id;
    public long budget;
    public float vote_average;
    public String title;
    public String backdrop_path;
    public String overview;
    public String release_date;
    public String poster_path;
    public String homepage;
    public ArrayList<GenersModel> genres;
}
