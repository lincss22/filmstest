package soft_team.filmstest.Server.servermodels;

import java.io.Serializable;

public class GetResults implements Serializable {
    public int id;
    public boolean video;
    public float vote_average;
    public String title;
    public String backdrop_path;
    public String overview;
    public String release_date;
}
