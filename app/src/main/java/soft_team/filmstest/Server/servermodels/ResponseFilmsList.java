package soft_team.filmstest.Server.servermodels;

import java.util.ArrayList;

public class ResponseFilmsList {
    public int page;
    public int total_results;
    public int total_pages;
    public ArrayList<GetResults> results;
}
