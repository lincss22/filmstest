package soft_team.filmstest.Server;

import okhttp3.OkHttpClient;

import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
import soft_team.filmstest.Server.servermodels.GetResults;
import soft_team.filmstest.Server.servermodels.ResponseDetails;
import soft_team.filmstest.Server.servermodels.ResponseFilmsList;

public class Transport {
        public static final String HOST = "https://api.themoviedb.org";
        public static final String URL = "https://image.tmdb.org/t/p/w500";
        public static final String BASE_URL = HOST;
        public interface TransportService {
            @GET("/3/movie/popular")
            Call<ResponseFilmsList> getFilms(
                    @Query("api_key") String apikey,
                    @Query("page") int page);

            @GET("/3/movie/{movie_id}")
            Call<ResponseDetails> getDetails(
                    @Path("movie_id") int movie_id,
                    @Query("api_key") String apikey);
        }

        static TransportService newInstance() {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(HOST)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(TransportService.class);
        }
}
