package soft_team.filmstest.Server;

import soft_team.filmstest.Realm.models.DetailsFilmModel;
import soft_team.filmstest.Server.servermodels.ResponseDetails;
import soft_team.filmstest.Server.servermodels.ResponseFilmsList;
import soft_team.filmstest.Utils.MainApplication;
import soft_team.filmstest.Utils.MainApplication_;
import soft_team.filmstest.Utils.Settings;

import static soft_team.filmstest.Utils.Settings.getApikey;

public class RequestManager {
    private static RequestManager mInstance;
    private final Transport.TransportService mService;
    private RequestManager() {
        mService = TransportHolder.getServiceInstance();
    }

    public static RequestManager getInstance() {
        synchronized (RequestManager.class) {
            if(mInstance == null) {
                mInstance = new RequestManager();
            }
        }
        return mInstance;
    }
    public ResponseFilmsList getFilms(int page){
        try {
            return mService.getFilms(Settings.getApikey(MainApplication_.getInstance().getApplicationContext()),page).execute().body();
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
    public ResponseDetails getDetails(int id){
        try {
            return mService.getDetails(id,Settings.getApikey(MainApplication_.getInstance().getApplicationContext())).execute().body();
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
}
