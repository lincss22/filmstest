package soft_team.filmstest.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import soft_team.filmstest.R;
import soft_team.filmstest.Realm.RealmManager;
import soft_team.filmstest.Realm.models.DetailsFilmModel;
import soft_team.filmstest.Realm.models.FilmsListModel;
import soft_team.filmstest.Server.RequestManager;
import soft_team.filmstest.Server.Transport;
import soft_team.filmstest.Server.servermodels.GetResults;
import soft_team.filmstest.Server.servermodels.ResponseDetails;
import soft_team.filmstest.Utils.Utils;

@EActivity(R.layout.detail_activity)
public class DetailActivity extends AppCompatActivity {
    @ViewById
    FlowingDrawer drawerlayout;
    @ViewById
    View fab;
    @ViewById
    TextView open;
    @ViewById
    TextView budget;
    @ViewById
    TextView vote_average;
    @ViewById
    TextView title;
    @ViewById
    ImageView backdrop_path;
    @ViewById
    TextView overview;
    @ViewById
    TextView release_date;
    @ViewById
    TextView genres;
    @ViewById
    TextView homepage;
    ProgressDialog progressDialog;

    @Extra
    GetResults filmData;
    @Extra
    FilmsListModel filmDataSaved;
    @Extra
    boolean stored;
    DetailsFilmModel detailsFilm = new DetailsFilmModel();
    FilmsListModel filmsListModel = new FilmsListModel();

    @AfterViews
    void initActivity(){
        if(stored){
            if(filmDataSaved.getDetailsFilmModel() != null){
                detailsFilm = filmDataSaved.getDetailsFilmModel();
                fillViews();
            } else {
                progressDialog = new ProgressDialog(this);
                progressDialog.setTitle(R.string.please_wait_message);
                progressDialog.setCancelable(false);
                progressDialog.show();
                filmsListModel = filmDataSaved;
                loadGetDetails();
            }
            fab.setVisibility(View.GONE);
        } else {
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(R.string.please_wait_message);
            progressDialog.setCancelable(false);
            progressDialog.show();
            loadGetDetails();
        }
        drawerlayout.setTouchMode(ElasticDrawer.TOUCH_MODE_BEZEL);
        drawerlayout.setOnDrawerStateChangeListener(new ElasticDrawer.OnDrawerStateChangeListener() {
            @Override
            public void onDrawerStateChange(int oldState, int newState) {
                Utils.checkButton(open);
            }

            @Override
            public void onDrawerSlide(float openRatio, int offsetPixels) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        MainActivity_.intent(this).start();
        finish();
    }

    @Click
    void open(){
        if(RealmManager.getInstance().getFilmProfiles() != null && !RealmManager.getInstance().getFilmProfiles().isEmpty()){
            boolean on = true;
            MainActivity_.intent(this).extra("on",on).start();
            finish();
        }
    }

    void fillViews(){
        if(!TextUtils.isEmpty(String.valueOf(detailsFilm.getBudget()))){
            budget.setText(String.valueOf(detailsFilm.getBudget())+" $");
        }
        if(!TextUtils.isEmpty(detailsFilm.getTitle())){
            title.setText(detailsFilm.getTitle());
        }
        if(!TextUtils.isEmpty(detailsFilm.getRelease_date())){
            release_date.setText("("+detailsFilm.getRelease_date()+")");
        }
        if(!TextUtils.isEmpty(String.valueOf(detailsFilm.getVote_average()))){
            vote_average.setText(String.valueOf(detailsFilm.getVote_average()));
        }
        if(!TextUtils.isEmpty(detailsFilm.getGenres())){
            genres.setText(detailsFilm.getGenres());
        }
        if(!TextUtils.isEmpty(detailsFilm.getOverview())){
            overview.setText(detailsFilm.getOverview());
        }
        if(!TextUtils.isEmpty(detailsFilm.getHomepage())){
            homepage.setText(detailsFilm.getHomepage());
        }
        if(!TextUtils.isEmpty(detailsFilm.getPoster_path())){
            Picasso
                    .with(this)
                    .load(Transport.URL + detailsFilm.getPoster_path())
                    .centerInside()
                    .fit()
                    .into(backdrop_path);
        }
        if(progressDialog != null){
            progressDialog.dismiss();
        }
    }

    @Click
    void homepage(){
        if(!TextUtils.isEmpty(detailsFilm.getHomepage())){
            Uri uri = Uri.parse(detailsFilm.getHomepage());
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
    }

    @Background
    void loadGetDetails() {
        if(filmData != null){
            onGetDetails(RequestManager.getInstance().getDetails(filmData.id));
        } else {
            onGetDetails(RequestManager.getInstance().getDetails(filmDataSaved.getId()));
        }
    }

    @UiThread
    void onGetDetails(ResponseDetails responseDetails){
        if(responseDetails != null){
            detailsFilm = Utils.transform(responseDetails);
            filmsListModel = Utils.transform(filmData,detailsFilm);
            fillViews();
            if(filmDataSaved != null){
                filmDataSaved.setDetailsFilmModel(detailsFilm);
                RealmManager.getInstance().saveDetails(detailsFilm);
                RealmManager.getInstance().saveFilmsList(filmDataSaved);
            }
        } else {
            if(progressDialog != null){
                progressDialog.dismiss();
            }
            Toast.makeText(this,"Some error with network connection", Toast.LENGTH_SHORT).show();
        }
    }
    @Click
    void fab(){
        RealmManager.getInstance().saveDetails(detailsFilm);
        RealmManager.getInstance().saveFilmsList(Utils.transform(filmData,detailsFilm));
        Toast.makeText(this,"Saved", Toast.LENGTH_SHORT).show();
    }
}
