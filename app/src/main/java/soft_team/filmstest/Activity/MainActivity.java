package soft_team.filmstest.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ItemLongClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import soft_team.filmstest.Adapter.FilmListAdapter;
import soft_team.filmstest.Adapter.StoredFilmsAdapter;
import soft_team.filmstest.R;
import soft_team.filmstest.Realm.RealmManager;
import soft_team.filmstest.Realm.models.DetailsFilmModel;
import soft_team.filmstest.Realm.models.FilmsListModel;
import soft_team.filmstest.Server.RequestManager;
import soft_team.filmstest.Server.Transport;
import soft_team.filmstest.Server.servermodels.GetResults;
import soft_team.filmstest.Server.servermodels.ResponseFilmsList;
import soft_team.filmstest.Utils.Settings;
import soft_team.filmstest.Utils.Utils;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {
    @ViewById
    FlowingDrawer drawerlayout;
    @ViewById
    TextView open;
    @ViewById
    TextView backList;
    @ViewById
    TextView empty;
    @ViewById
    ListView list;
    FilmListAdapter adapter;
    StoredFilmsAdapter adapterStored;
    public ProgressDialog progressDialog;
    FilmsListModel filmsListModel;
    int page = 1;
    @Extra
    boolean on;

    boolean started;

    @AfterViews
    public void initActivity(){
        Settings.setApikey(this,"a4450997bdf04290fa6b606066382d55");
        drawerlayout.setTouchMode(ElasticDrawer.TOUCH_MODE_BEZEL);
        if(on){
            adapterStored = new StoredFilmsAdapter(this);
            adapterStored.addAll(RealmManager.getInstance().getFilmProfiles());
            list.setAdapter(adapterStored);
        } else {
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(R.string.please_wait_message);
            progressDialog.setCancelable(false);
            progressDialog.show();
            loadGetFilms();
        }
        drawerlayout.setOnDrawerStateChangeListener(new ElasticDrawer.OnDrawerStateChangeListener() {
            @Override
            public void onDrawerStateChange(int oldState, int newState) {

            }

            @Override
            public void onDrawerSlide(float openRatio, int offsetPixels) {
                Utils.checkButton(open);
                Utils.checkButton(backList);
            }
        });
        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    View v = list.getChildAt(0);
                    int offset = (v == null) ? 0 : v.getTop();
                    if (offset == 0) {

                    }
                } else if (totalItemCount - visibleItemCount == firstVisibleItem){
                    View v =  list.getChildAt(totalItemCount-1);
                        int offset = (v == null) ? 0 : v.getTop();
                        if (offset == 0) {
                            if(!started){
                                page ++;
                                started = true;
                                loadGetFilms();
                                progressDialog = new ProgressDialog(MainActivity.this);
                                progressDialog.setTitle(R.string.please_wait_message);
                                progressDialog.setCancelable(false);
                                progressDialog.show();
                            }
                        return;
                    }
                }
            }
        });
    }

    @ItemClick
    void list(int position){
        final GetResults model = adapter.getItem(position);
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
        alert.setTitle(null);
        alert.setMessage(R.string.message);
        alert.setPositiveButton(R.string.my_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DetailActivity_.intent(MainActivity.this).extra("filmId",model.id).extra("filmData",model).start();
                finish();
            }
        });
        alert.setNeutralButton(R.string.save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RealmManager.getInstance().saveFilmsList(Utils.transform(model,null));
                Toast.makeText(MainActivity.this,"Saved", Toast.LENGTH_SHORT).show();
            }
        });
        alert.setNegativeButton(R.string.my_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    @Click
    void open(){
        if(RealmManager.getInstance().getFilmProfiles() != null && !RealmManager.getInstance().getFilmProfiles().isEmpty()){
            on = true;
            initActivity();
        }
    }

    @Click
    void backList(){
        if(RealmManager.getInstance().getFilmProfiles() != null && !RealmManager.getInstance().getFilmProfiles().isEmpty()){
            if(on){
                on = false;
            }
            initActivity();
        }
    }

    @Background
    void loadGetFilms() {
        onGetFilms(RequestManager.getInstance().getFilms(page));
    }

    @UiThread
    void onGetFilms(ResponseFilmsList responseFilmsList){
        if(responseFilmsList != null){
            started = false;
            adapter = new FilmListAdapter(this);
            adapter.addAll(responseFilmsList.results);
            list.setAdapter(adapter);
            if(progressDialog != null){
                progressDialog.dismiss();
            }
        } else {
            if(progressDialog != null){
                progressDialog.dismiss();
            }
            Toast.makeText(this,"Some error with network connection", Toast.LENGTH_SHORT).show();
            on = true;
            initActivity();
        }
    }
}
