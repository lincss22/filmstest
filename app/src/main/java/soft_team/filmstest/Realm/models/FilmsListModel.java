package soft_team.filmstest.Realm.models;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FilmsListModel extends RealmObject implements Serializable {
    @PrimaryKey
    private String MyId;

    private int id;
    private float vote_average;
    private String title;
    private String backdrop_path;
    private String overview;
    private String release_date;
    private DetailsFilmModel detailsFilmModel;
    public DetailsFilmModel getDetailsFilmModel() {
        return detailsFilmModel;
    }

    public void setDetailsFilmModel(DetailsFilmModel detailsFilmModel) {
        this.detailsFilmModel = detailsFilmModel;
    }

    public String getMyId() {
        return MyId;
    }

    public void setMyId(String myId) {
        MyId = myId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getVote_average() {
        return vote_average;
    }

    public void setVote_average(float vote_average) {
        this.vote_average = vote_average;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }
}
