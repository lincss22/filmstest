package soft_team.filmstest.Realm;

import android.content.Context;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;
import soft_team.filmstest.Realm.models.DetailsFilmModel;
import soft_team.filmstest.Realm.models.FilmsListModel;
import soft_team.filmstest.Utils.MainApplication_;

public class RealmManager {
    private static Context mContext;
    private static RealmManager mInstance;
    public static RealmManager getInstance() {
        mContext = MainApplication_.getInstance().getApplicationContext();
        synchronized (RealmManager.class) {
            if(mInstance == null) {
                mInstance = new RealmManager();
            }
        }
        return mInstance;
    }

    public void saveFilmsList(FilmsListModel model) {
        Realm realm = null;
        try {
            if (TextUtils.isEmpty(model.getMyId())) {
                model.setMyId(UUID.randomUUID()+"");
            }
            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(model);
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public void saveDetails(DetailsFilmModel model) {
        Realm realm = null;
        try {
            if (TextUtils.isEmpty(model.getMyId())) {
                model.setMyId(UUID.randomUUID()+"");
            }
            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(model);
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public ArrayList<FilmsListModel> getFilmProfiles() {
        ArrayList<FilmsListModel> model = null;
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            RealmResults<FilmsListModel> realmResults = realm
                    .where(FilmsListModel.class)
                    .findAll();
            realm.commitTransaction();
            model = cloneShopItems(realmResults);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
        return model;
    }

    public void deleteFilm(String id) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            FilmsListModel realmResults = realm
                    .where(FilmsListModel.class)
                    .equalTo("MyId", id)
                    .findFirst();
            realmResults.removeFromRealm();
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    private ArrayList<FilmsListModel> cloneShopItems(RealmResults<FilmsListModel> model) {
        ArrayList<FilmsListModel> address = new ArrayList<>();
        for (FilmsListModel item : model) {
            address.add(clone(item));
        }
        return address;
    }

    private FilmsListModel clone(FilmsListModel model) {
        try {
            FilmsListModel address = new FilmsListModel();
            address.setMyId(model.getMyId());
            address.setId(model.getId());
            address.setBackdrop_path(model.getBackdrop_path());
            address.setVote_average(model.getVote_average());
            address.setTitle(model.getTitle());
            address.setOverview(model.getOverview());
            address.setRelease_date(model.getRelease_date());
            address.setDetailsFilmModel(clone(model.getDetailsFilmModel()));
            return address;
        } catch (Exception ex) {
            return null;
        }
    }

    private DetailsFilmModel clone(DetailsFilmModel model) {
        try {
            DetailsFilmModel address = new DetailsFilmModel();
            address.setMyId(model.getMyId());
            address.setId(model.getId());
            address.setBackdrop_path(model.getBackdrop_path());
            address.setVote_average(model.getVote_average());
            address.setTitle(model.getTitle());
            address.setOverview(model.getOverview());
            address.setRelease_date(model.getRelease_date());
            address.setBudget(model.getBudget());
            address.setGenres(model.getGenres());
            address.setPoster_path(model.getPoster_path());
            address.setHomepage(model.getHomepage());
            return address;
        } catch (Exception ex) {
            return null;
        }
    }
}
